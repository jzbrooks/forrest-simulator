import turtle
import random

class Fish:
    """
    Creates a fish to live in the world
    Born in water.
    Cannot leave river.
    Eats plants
    """
    def __init__(self):
        self.turtle = turtle.Turtle()
        self.turtle.up()
        self.turtle.hideturtle()
        self.turtle.shape("MagiFish.gif")

        self.xpos = 0
        self.ypos = 0
        self.world = None

        self.energy = 50
        self.starveTick = 0
        self.breedTick = 0

    def setX(self,newx):
        self.xpos = newx

    def setY(self,newy):
        self.ypos = newy

    def getX(self):
        return self.xpos

    def getY(self):
        return self.ypos

    def setWorld(self,aworld):
        self.world = aworld

    def appear(self):
        self.turtle.goto(self.xpos, self.ypos)
        self.turtle.showturtle()
        self.world.incFish()

    def hide(self):
        self.turtle.hideturtle()
        self.world.decFish()

    def move(self,newx,newy):
        self.world.moveThing(self.xpos,self.ypos,newx,newy)
        self.xpos = newx
        self.ypos = newy
        self.turtle.goto(self.xpos, self.ypos)

    def liveALittle(self):
        offsetList = [(-1,1) ,(0,1) ,(1,1),
                      (-1,0)        ,(1,0),
                      (-1,-1),(0,-1),(1,-1)]
        adjfish = 0
        for offset in offsetList:
            newx = self.xpos + offset[0]
            newy = self.ypos + offset[1]
            if 0 <= newx < self.world.getMaxX()  and  0 <= newy < self.world.getMaxY():
                if (not self.world.emptyLocation(newx,newy)) and \
                        isinstance(self.world.lookAtLocation(newx,newy),Fish):
                    adjfish = adjfish + 1

        if adjfish >= 3:
            self.world.delThing(self)
        else:
            self.tryToEat()

            self.breedTick = self.breedTick + 1

            if self.breedTick >= 12 and self.energy > 10:
                self.tryToBreed()

        if self.energy <= 2: # if energy is two then moving will "kill" it anyway.
            self.world.delThing(self)
        else:
            self.tryToMove()

    def tryToBreed(self):
        offsetList = [(-1,1) ,(0,1) ,(1,1),
                      (-1,0)        ,(1,0),
                      (-1,-1),(0,-1),(1,-1)]
        randomOffsetIndex = random.randrange(len(offsetList))
        randomOffset = offsetList[randomOffsetIndex]
        nextx = self.xpos + randomOffset[0]
        nexty = self.ypos + randomOffset[1]
        while not (0 <= nextx < self.world.getMaxX() and
                   0 <= nexty < self.world.getMaxY() ):
            randomOffsetIndex = random.randrange(len(offsetList))
            randomOffset = offsetList[randomOffsetIndex]
            nextx = self.xpos + randomOffset[0]
            nexty = self.ypos + randomOffset[1]

        if self.world.emptyLocation(nextx,nexty) and (self.world.riverY1 < nexty):
           childThing = Fish()
           self.world.addThing(childThing,nextx,nexty)
           self.energy -= 10
           self.breedTick = 0

    def tryToMove(self):
        offsetList = [(-1,1) ,(0,1) ,(1,1),
                      (-1,0)        ,(1,0),
                      (-1,-1),(0,-1),(1,-1)]
        randomOffsetIndex = random.randrange(len(offsetList))
        randomOffset = offsetList[randomOffsetIndex]
        nextx = self.xpos + randomOffset[0]
        nexty = self.ypos + randomOffset[1]
        while not(0 <= nextx < self.world.getMaxX() and
                  0 <= nexty < self.world.getMaxY() ):
            randomOffsetIndex = random.randrange(len(offsetList))
            randomOffset = offsetList[randomOffsetIndex]
            nextx = self.xpos + randomOffset[0]
            nexty = self.ypos + randomOffset[1]

        if self.world.emptyLocation(nextx,nexty) and (self.world.riverY1 < nexty):
            self.move(nextx,nexty)
            self.energy -= 2

    def tryToEat(self):
        offsetList = [(-1,1) ,(0,1) ,(1,1),
                      (-1,0)        ,(1,0),
                      (-1,-1),(0,-1),(1,-1)]
        adjprey = []
        for offset in offsetList:
            newx = self.xpos + offset[0]
            newy = self.ypos + offset[1]
            if 0 <= newx < self.world.getMaxX() and 0 <= newy < self.world.getMaxY():
                if not self.world.emptyLocation(newx,newy) and isinstance(self.world.lookAtLocation(newx,newy),Plant):
                    adjprey.append(self.world.lookAtLocation(newx, newy))

        if len(adjprey)>0:
            randomprey = adjprey[random.randrange(len(adjprey))]
            preyx = randomprey.getX()
            preyy = randomprey.getY()

            self.world.delThing(randomprey)
            self.move(preyx, preyy)
            self.energy += 5


class Bear:
    """
    Creates a bear to live in the world
    Born on land
    Can swim to eat fish.
    """
    def __init__(self):
        self.turtle = turtle.Turtle()
        self.turtle.up()
        self.turtle.hideturtle()
        self.turtle.shape("Teddiursa.gif")

        self.xpos = 0
        self.ypos = 0
        self.world = None

        self.energy = 50
        self.starveTick = 0
        self.breedTick = 0

    def setX(self,newx):
        self.xpos = newx

    def setY(self,newy):
        self.ypos = newy

    def getX(self):
        return self.xpos

    def getY(self):
        return self.ypos

    def setWorld(self,aworld):
        self.world = aworld

    def appear(self):
        self.turtle.goto(self.xpos, self.ypos)
        self.turtle.showturtle()
        self.world.incBear()

    def hide(self):
        self.turtle.hideturtle()
        self.world.decBear()

    def move(self,newx,newy):
        self.world.moveThing(self.xpos,self.ypos,newx,newy)
        self.xpos = newx
        self.ypos = newy
        self.turtle.goto(self.xpos, self.ypos)

    def liveALittle(self):
        self.breedTick = self.breedTick + 1
        if self.breedTick >= 8 and self.energy >= 10:
            self.tryToBreed()

        self.tryToEat()

        if self.energy > 0:
            self.tryToMove()
        else:
            self.world.delThing(self)

    def tryToEat(self):
        offsetList = [(-1,1) ,(0,1) ,(1,1),
                      (-1,0)        ,(1,0),
                      (-1,-1),(0,-1),(1,-1)]
        adjprey = []
        for offset in offsetList:
            newx = self.xpos + offset[0]
            newy = self.ypos + offset[1]
            if 0 <= newx < self.world.getMaxX() and 0 <= newy < self.world.getMaxY():
                if (not self.world.emptyLocation(newx,newy)) and isinstance(self.world.lookAtLocation(newx,newy),Fish):
                    adjprey.append(self.world.lookAtLocation(newx,newy))
                if (not self.world.emptyLocation(newx,newy)) and isinstance(self.world.lookAtLocation(newx,newy),Berry):
                    adjprey.append(self.world.lookAtLocation(newx,newy))

        if len(adjprey)>0:
            randomprey = adjprey[random.randrange(len(adjprey))]
            preyx = randomprey.getX()
            preyy = randomprey.getY()

            if (isinstance(randomprey,Fish)):
                self.energy+= 10
            if (isinstance(randomprey,Berry)):
                self.energy+= 4

            self.world.delThing(randomprey)
            self.move(preyx,preyy)

    def tryToBreed(self):
        offsetList = [(-1,1) ,(0,1) ,(1,1),
                      (-1,0)        ,(1,0),
                      (-1,-1),(0,-1),(1,-1)]
        randomOffsetIndex = random.randrange(len(offsetList))
        randomOffset = offsetList[randomOffsetIndex]
        nextx = self.xpos + randomOffset[0]
        nexty = self.ypos + randomOffset[1]
        while not (0 <= nextx < self.world.getMaxX() and
                   0 <= nexty < self.world.getMaxY() ):
            randomOffsetIndex = random.randrange(len(offsetList))
            randomOffset = offsetList[randomOffsetIndex]
            nextx = self.xpos + randomOffset[0]
            nexty = self.ypos + randomOffset[1]

        if self.world.emptyLocation(nextx,nexty):
           childThing = Bear()
           self.world.addThing(childThing,nextx,nexty)
           self.breedTick = 0
           self.energy -= 13

    def tryToMove(self):
        offsetList = [(-1,1) ,(0,1) ,(1,1),
                      (-1,0)        ,(1,0),
                      (-1,-1),(0,-1),(1,-1)]
        randomOffsetIndex = random.randrange(len(offsetList))
        randomOffset = offsetList[randomOffsetIndex]
        nextx = self.xpos + randomOffset[0]
        nexty = self.ypos + randomOffset[1]
        while not(0 <= nextx < self.world.getMaxX() and
                  0 <= nexty < self.world.getMaxY() ):
            randomOffsetIndex = random.randrange(len(offsetList))
            randomOffset = offsetList[randomOffsetIndex]
            nextx = self.xpos + randomOffset[0]
            nexty = self.ypos + randomOffset[1]

        if self.world.emptyLocation(nextx,nexty):
           self.move(nextx,nexty)
           self.energy -= 2

class Plant:
    """
    Creates a plant to live in the world
    Can be eaten by fish.
    Only grows in water.
    """
    def __init__(self):
        self.turtle = turtle.Turtle()
        self.turtle.up()
        self.turtle.hideturtle()
        self.turtle.shape("OddPlant.gif")

        self.xpos = 0
        self.ypos = 0
        self.world = None

        self.starveTick = 0
        self.breedTick = 0

    def setX(self,newx):
        self.xpos = newx

    def setY(self,newy):
        self.ypos = newy

    def getX(self):
        return self.xpos

    def getY(self):
        return self.ypos

    def setWorld(self,aworld):
        self.world = aworld

    def appear(self):
        self.turtle.goto(self.xpos, self.ypos)
        self.turtle.showturtle()
        self.world.incPlant()

    def hide(self):
        self.turtle.hideturtle()
        self.world.decPlant()

    def move(self,newx,newy):
        self.world.moveThing(self.xpos,self.ypos,newx,newy)
        self.xpos = newx
        self.ypos = newy
        self.turtle.goto(self.xpos, self.ypos)

    def liveALittle(self):
        offsetList = [(-1,1) ,(0,1) ,(1,1),
                      (-1,0)        ,(1,0),
                      (-1,-1),(0,-1),(1,-1)]
        adjPlant = 0
        for offset in offsetList:
            newx = self.xpos + offset[0]
            newy = self.ypos + offset[1]
            if 0 <= newx < self.world.getMaxX()  and  0 <= newy < self.world.getMaxY():
                if (not self.world.emptyLocation(newx,newy)) and isinstance(self.world.lookAtLocation(newx,newy),Plant):
                    adjPlant += 1

        if adjPlant >= 3:
            self.world.delThing(self)

        self.breedTick = self.breedTick + 1
        if self.breedTick >= 5:
            self.tryToBreed()

    def tryToBreed(self):
        offsetList = [(-1,1) ,(0,1) ,(1,1),
                      (-1,0)        ,(1,0),
                      (-1,-1),(0,-1),(1,-1)]
        randomOffsetIndex = random.randrange(len(offsetList))
        randomOffset = offsetList[randomOffsetIndex]
        nextx = self.xpos + randomOffset[0]
        nexty = self.ypos + randomOffset[1]
        while not (0 <= nextx < self.world.getMaxX() and
                   0 <= nexty < self.world.getMaxY() ):
            randomOffsetIndex = random.randrange(len(offsetList))
            randomOffset = offsetList[randomOffsetIndex]
            nextx = self.xpos + randomOffset[0]
            nexty = self.ypos + randomOffset[1]

        if self.world.emptyLocation(nextx,nexty) and (self.world.riverY1 < nexty):
           childThing = Plant()
           self.world.addThing(childThing,nextx,nexty)
           self.breedTick = 0

class Berry:
    """
    Creates a berry plant to live in the world.
    Can be eaten by bears.
    Only grows on land.
    """
    def __init__(self):
        self.turtle = turtle.Turtle()
        self.turtle.up()
        self.turtle.hideturtle()
        self.turtle.shape("Berries.gif")

        self.xpos = 0
        self.ypos = 0
        self.world = None

        self.starveTick = 0
        self.breedTick = 0

    def setX(self,newx):
        self.xpos = newx

    def setY(self,newy):
        self.ypos = newy

    def getX(self):
        return self.xpos

    def getY(self):
        return self.ypos

    def setWorld(self,aworld):
        self.world = aworld

    def appear(self):
        self.turtle.goto(self.xpos, self.ypos)
        self.turtle.showturtle()
        self.world.incBerry()

    def hide(self):
        self.turtle.hideturtle()
        self.world.decBerry()

    def move(self,newx,newy):
        self.world.moveThing(self.xpos,self.ypos,newx,newy)
        self.xpos = newx
        self.ypos = newy
        self.turtle.goto(self.xpos, self.ypos)

    def liveALittle(self):
        offsetList = [(-1,1) ,(0,1) ,(1,1),
                      (-1,0)        ,(1,0),
                      (-1,-1),(0,-1),(1,-1)]
        adjBerry = 0
        for offset in offsetList:
            newx = self.xpos + offset[0]
            newy = self.ypos + offset[1]
            if 0 <= newx < self.world.getMaxX()  and  0 <= newy < self.world.getMaxY():
                if (not self.world.emptyLocation(newx,newy)) and isinstance(self.world.lookAtLocation(newx,newy),Berry):
                    adjBerry += 1

        if adjBerry >= 3:
            self.world.delThing(self)

        self.breedTick = self.breedTick + 1
        if self.breedTick >= 5:
            self.tryToBreed()

    def tryToBreed(self):
        offsetList = [(-1,1) ,(0,1) ,(1,1),
                      (-1,0)        ,(1,0),
                      (-1,-1),(0,-1),(1,-1)]
        randomOffsetIndex = random.randrange(len(offsetList))
        randomOffset = offsetList[randomOffsetIndex]
        nextx = self.xpos + randomOffset[0]
        nexty = self.ypos + randomOffset[1]
        while not (0 <= nextx < self.world.getMaxX() and
                   0 <= nexty < self.world.getMaxY() ):
            randomOffsetIndex = random.randrange(len(offsetList))
            randomOffset = offsetList[randomOffsetIndex]
            nextx = self.xpos + randomOffset[0]
            nexty = self.ypos + randomOffset[1]

        if self.world.emptyLocation(nextx,nexty)  and (self.world.riverY1 > nexty):
           childThing = Berry()
           self.world.addThing(childThing,nextx,nexty)
           self.breedTick = 0