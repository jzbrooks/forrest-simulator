import random
from world import World
from creatures import Bear, Fish, Plant, Berry
import turtle

# Generate Creature histogram
def genAnimalPlot(filename):
    """
    Creates a histogram over 10 intervals
    in a set of world simulation data
    """
    file = open(filename,"r")
    screen = turtle.Screen()

    # Created a turtle for each of the counts
    bearTurtle = turtle.Turtle()
    fishTurtle = turtle.Turtle()
    plantTurtle = turtle.Turtle()
    berryTurtle = turtle.Turtle()

    # Variables used in plot generating process
    maxItem = 0
    creatureItems = []
    subsetCreatures = []
    maxX = 10

    for line in file:
        l = file.readline()
        tokens = l.split()
        creatureItems += [tokens]
    file.close()

    # Generates a subset of creature items to draw
    # also finds the max in subsetCreatures[i][1:] for all i
    idx = 0
    for i in range(0, len(creatureItems), len(creatureItems)//maxX):
        subsetCreatures += [creatureItems[i]]
        for j in range(1,5):
            if int(subsetCreatures[idx][j]) > maxItem:
                maxItem = int(subsetCreatures[idx][j])
        idx += 1

    # Setting up chart drawing mechanisms
    chartT = turtle.Turtle()
    screen.setworldcoordinates(-1,-6,maxX + 1, maxItem + 1)

    bearTurtle.color("saddle brown")
    fishTurtle.color("orange")
    plantTurtle.color("green")
    berryTurtle.color("pink")
    bearTurtle.width(4)
    fishTurtle.width(4)
    berryTurtle.width(4)
    plantTurtle.width(4)
    bearTurtle.speed(0)
    fishTurtle.speed(0)
    plantTurtle.speed(0)
    berryTurtle.speed(0)
    bearTurtle.ht()
    plantTurtle.ht()
    fishTurtle.ht()
    berryTurtle.ht()

    chartT.ht()
    chartT.speed(0)
    chartT.up()
    chartT.goto(0,0)
    chartT.down()
    chartT.goto(maxX,0)
    chartT.up()

    chartT.goto(-1,0)
    chartT.write("0", font=("Helvetica",16,"bold"))
    chartT.goto(-1,maxItem-2) # lowered location Y by 2 to compensate for font size
    chartT.write(str(maxItem), font=("Helvetica",16,"bold"))

    # Draw
    for i in range(0,len(subsetCreatures)):

        chartT.goto(i,-6)
        chartT.write(subsetCreatures[i][0],font=("Helvetica",16,"bold"))

        fishTurtle.up()
        fishTurtle.goto(i+0.1,0)
        fishTurtle.down()
        fishTurtle.goto(i+0.1,int(subsetCreatures[i][1]))

        bearTurtle.up()
        bearTurtle.goto(i,0)
        bearTurtle.down()
        bearTurtle.goto(i,int(subsetCreatures[i][2]))

        plantTurtle.up()
        plantTurtle.goto(i+0.2,0)
        plantTurtle.down()
        plantTurtle.goto(i+0.2,int(subsetCreatures[i][3]))

        berryTurtle.up()
        berryTurtle.goto(i+0.3,0)
        berryTurtle.down()
        berryTurtle.goto(i+0.3,int(subsetCreatures[i][4]))

    screen.exitonclick()

# Main simulation method
def mainSimulation():
    """
    Main simulation function
    Initializaes world and runs it
    """
    numberOfBears = 10
    numberOfFish = 10
    numberOfPlants = 30
    numberOfBerries = 20
    worldLifeTime = 2500
    worldWidth = 50
    worldHeight = 25

    myworld = World(worldWidth,worldHeight, 15, worldHeight)
    myworld.draw()

    for i in range(numberOfFish):
        newfish = Fish()
        x = random.randrange(myworld.getMaxX())
        y = random.randrange(myworld.riverY1,myworld.riverY2)
        while not myworld.emptyLocation(x,y):
            x = random.randrange(myworld.getMaxX())
            y = random.randrange(myworld.riverY1, myworld.riverY2)
        myworld.addThing(newfish,x,y)

    for i in range(numberOfBears):
        newbear = Bear()
        x = random.randrange(myworld.getMaxX())
        y = random.randrange(myworld.riverY1)
        while not myworld.emptyLocation(x,y):
            x = random.randrange(myworld.getMaxX())
            y = random.randrange(myworld.riverY1)
        myworld.addThing(newbear,x,y)

    for i in range(numberOfPlants):
        newplant = Plant()
        x = random.randrange(myworld.getMaxX())
        y = random.randrange(myworld.riverY1, myworld.riverY2)
        while not myworld.emptyLocation(x,y):
            x = random.randrange(myworld.getMaxX())
            y = random.randrange(myworld.riverY1, myworld.riverY2)
        myworld.addThing(newplant,x,y)

    for i in range(numberOfBerries):
        newBerry = Berry()
        x = random.randrange(myworld.getMaxX())
        y = random.randrange(myworld.riverY1)
        while not myworld.emptyLocation(x,y):
            x = random.randrange(myworld.getMaxX())
            y = random.randrange(myworld.riverY1)
        myworld.addThing(newBerry,x,y)

    file = open("SimData.txt", "w")

    for i in range(worldLifeTime):
        myworld.liveALittle()
        file.write("%-8d %-8d %-8d %-8d %-8d \n" % \
                   (i, myworld.fishCount, myworld.bearCount, myworld.plantCount, myworld.berryCount))
    file.close()
    myworld.freezeWorld()

# When simulation window is closed, the histogram will be generated.
mainSimulation()
genAnimalPlot("SimData.txt")