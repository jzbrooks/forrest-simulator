import turtle
import random

class World:
    """
    Creates a world for creatures to live.

    Also keeps track of the numbers of each
    type of creature alive in the world.
    """
    def __init__(self, mx, my, river_y1, river_y2):
        self.maxX = mx
        self.maxY = my
        self.thingList = []
        self.grid = []
        self.fishCount = 0
        self.plantCount = 0
        self.bearCount = 0
        self.berryCount = 0
        self.riverY1 = river_y1
        self.riverY2 = river_y2

        for arow in range(self.maxY):
            row = []
            for acol in range(self.maxX):
                row.append(None)
            self.grid.append(row)

        self.wturtle = turtle.Turtle()
        self.wscreen = turtle.Screen()
        self.wscreen.setworldcoordinates(0,0,self.maxX-1,self.maxY-1)
        self.wscreen.addshape("Teddiursa.gif")
        self.wscreen.addshape("MagiFish.gif")
        self.wscreen.addshape("OddPlant.gif")
        self.wscreen.addshape("Berries.gif")
        self.wturtle.hideturtle()

    # Methods to keep track of creature counts
    def incFish(self):
        self.fishCount+=1
    def decFish(self):
        self.fishCount-=1
    def incBear(self):
        self.bearCount+=1
    def decBear(self):
        self.bearCount-=1
    def incPlant(self):
        self.plantCount+=1
    def decPlant(self):
        self.plantCount-=1
    def incBerry(self):
        self.berryCount+=1
    def decBerry(self):
        self.berryCount-=1


    def draw(self):
        self.wscreen.tracer(0)
        ### Draw land / river
        self.wturtle.color("burlywood")
        self.wturtle.begin_fill()
        self.wturtle.forward(self.maxX-1)
        self.wturtle.left(90)
        self.wturtle.forward(self.riverY1-1)
        self.wturtle.left(90)
        self.wturtle.forward(self.maxX-1)
        self.wturtle.left(90)
        self.wturtle.forward(self.riverY1-1)
        self.wturtle.left(90)
        self.wturtle.end_fill()

        self.wturtle.left(90)
        self.wturtle.forward(self.riverY1-1)
        self.wturtle.right(90)

        self.wturtle.color("lightblue")
        self.wturtle.begin_fill()
        self.wturtle.forward(self.maxX-1)
        self.wturtle.left(90)
        self.wturtle.forward(self.maxY - self.riverY1)
        self.wturtle.left(90)
        self.wturtle.forward(self.maxX-1)
        self.wturtle.left(90)
        self.wturtle.forward(self.maxY-1 - self.riverY1)
        self.wturtle.left(90)
        self.wturtle.end_fill()


        self.wturtle.goto(0,0)
        self.wturtle.color("black")


        self.wturtle.forward(self.maxX-1)
        self.wturtle.left(90)
        self.wturtle.forward(self.maxY-1)
        self.wturtle.left(90)
        self.wturtle.forward(self.maxX-1)
        self.wturtle.left(90)
        self.wturtle.forward(self.maxY-1)
        self.wturtle.left(90)
        for i in range(self.maxY-1):
            self.wturtle.forward(self.maxX-1)
            self.wturtle.backward(self.maxX-1)
            self.wturtle.left(90)
            self.wturtle.forward(1)
            self.wturtle.right(90)
        self.wturtle.forward(1)
        self.wturtle.right(90)
        for i in range(self.maxX-2):
            self.wturtle.forward(self.maxY-1)
            self.wturtle.backward(self.maxY-1)
            self.wturtle.left(90)
            self.wturtle.forward(1)
            self.wturtle.right(90)
        self.wscreen.tracer(1)

    def freezeWorld(self):
        self.wscreen.exitonclick()

    def addThing(self, athing, x, y):
        athing.setX(x)
        athing.setY(y)
        self.grid[y][x] = athing
        athing.setWorld(self)
        self.thingList.append(athing)
        athing.appear()

    def delThing(self,athing):
        athing.hide()
        self.grid[athing.getY()][athing.getX()] = None
        self.thingList.remove(athing)

    def moveThing(self,oldx,oldy,newx,newy):
        self.grid[newy][newx] = self.grid[oldy][oldx]
        self.grid[oldy][oldx] = None

    def getMaxX(self):
        return self.maxX

    def getMaxY(self):
        return self.maxY

    def liveALittle(self):
        """
        Chooses a random creature and calls liveALittle
        on the random creature.
        """
        if self.thingList != [ ]:
           athing = random.randrange(len(self.thingList))
           randomthing = self.thingList[athing]
           randomthing.liveALittle()

    def emptyLocation(self,x,y):
        if self.grid[y][x] == None:
            return True
        else:
            return False

    def lookAtLocation(self,x,y):
       return self.grid[y][x]



